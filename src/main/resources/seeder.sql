

# Intensity
INSERT INTO intensity (level)
VALUES ('Beginner'),
       ('Intermediate'),
       ('Advanced');

# Type
INSERT INTO type (name)
VALUES ('Bodybuilding'),
       ('Powerlifting'),
       ('Olympic Lifting'),
       ('Circuit'),
       ('Yoga'),
       ('P90X');

# Workout Option
INSERT INTO workout_option (workout)
VALUES ('A'),
       ('B'),
       ('C');

# News
INSERT INTO news (event, description)
VALUES ('FITCON: Day 1', 'Opening day! Come see your favorite fitness icons and get stocked up on all your favorite supplements, straight from the manufacturer themself!'),
       ('FITCON: Day 2', 'Day 2! We are hosting a plethora of fitness competitions today, come in, get pumped, and support our athletes!'),
       ('FITCON: Day 3', 'Today is all about book signings, meeting your fitness idols, and finishing up FITCON with an awards ceremony!'),
       ('Annual Ogden Marathon', 'May 18, 2019: Bring shoes and water'),
       ('Local Trail Cleanup', 'Starting at the X trailhead, we are going to hike the X miles to the top and pick up any trash we can find!'),
       ('Lance Armstrong Caught Dealing Steroids Again!', 'Will he ever learn?'),
       ('The definition of the kilogram has changed!', 'What does this mean to weight lifters everywhere?'),
       ('Rock Climbing Group', 'We are meeting at The Front in Ogden and enjoying some rock climbing madness!'),
       ('RAGNAR Race', 'Starts at X location and goes to X location. Bring shoes.'),
       ('Spartan Race', 'We are racing and stuff.'),
       ('Utes vs. BYU', 'Go Utes!'),
       ('Arnold Schwarzenegger', 'Book Signing'),
       ('Jillian Michaels', 'Book Signing'),
       ('Bodybuilding competition', 'utah');

# ##### Bodybuilding Workouts #####

# Beginner Bodybuilding 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Incline Bench', '../assets/images/INCLINEBarbell-Bench-Press.png', 1, 1, 1, 3, 12, 0, 'Push with your arms.'),
       ('Decline Bench', '../assets/images/DECLINEBarbell-Bench-Press.png', 1, 1, 1, 3, 12, 0, 'Push with your arms.'),
       ('Flat Bench',    '../assets/images/FLATBarbell-Bench-Press.png', 1, 1, 1, 3, 12, 0, 'Push with your arms.'),
       ('Cable Flyes',   '../assets/images/Cable\ Flies.png', 1, 1, 1, 3, 12, 0, 'Pull and then push with your arms.');

# Beginner Bodybuilding 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',      '../assets/images/Barbell\ Squat.png', 1, 1, 2, 3, 12, 0, 'Push with your legs.'),
       ('Leg Press',  '../assets/images/Leg\ Press.png', 1, 1, 2, 3, 12, 0, 'Push with your legs.'),
       ('Calf Raise', '../assets/images/Calf\ Raises.png', 1, 1, 2, 3, 12, 0, 'Push with your legs.'),
       ('Leg Curls',  '../assets/images/Leg\ Curls.png', 1, 1, 2, 3, 12, 0, 'Curl with your legs.');

# Beginner Bodybuilding 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Seated Rows',   '../assets/images/Seated\ Rows.png', 1, 1, 3, 3, 12, 0, 'Pull with your arms.'),
       ('Pullups',       '../assets/images/underhand\ pull-up.jpg', 1, 1, 3, 3, 12, 0, 'Pull up with your arms.'),
       ('Deadlifts',     '../assets/images/Deadlift.png', 1, 1, 3, 3, 12, 0, 'Pick it up.'),
       ('Dumbbell Rows', '../assets/images/Kneeling\ Dumbbell\ Row.png', 1, 1, 3, 3, 12, 0, 'Pull that dumbbell.');


# Intermediate Bodybuilding 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Incline Bench', '../assets/images/INCLINEBarbell-Bench-Press.png', 2, 1, 1, 3, 8, 2, 'Push with your arms.'),
       ('Decline Bench', '../assets/images/DECLINEBarbell-Bench-Press.png', 2, 1, 1, 3, 8, 2, 'Push with your arms.'),
       ('Flat Bench',    '../assets/images/FLATBarbell-Bench-Press.png', 2, 1, 1, 3, 8, 2, 'Push with your arms.'),
       ('Cable Flyes',   '../assets/images/Cable\ Flies.png', 2, 1, 1, 3, 8, 2, 'Push with your arms.');

# Intermediate Bodybuilding 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',      '../assets/images/Barbell\ Squat.png', 2, 1, 2, 3, 8, 2, 'Push with your legs.'),
       ('Leg Press',  '../assets/images/Leg\ Press.png', 2, 1, 2, 3, 8, 2, 'Push with your legs.'),
       ('Calf Raise', '../assets/images/Calf\ Raises.png', 2, 1, 2, 3, 8, 2, 'Push with your legs.'),
       ('Leg Curls',  '../assets/images/Leg\ Curls.png', 2, 1, 2, 3, 8, 2, 'Push with your legs.');

# Intermediate Bodybuilding 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Seated Rows',   '../assets/images/Seated\ Rows.png', 2, 1, 3, 3, 8, 2, 'Pull with your arms.'),
       ('Pullups',       '../assets/images/underhand\ pull-up.jpg', 2, 1, 3, 3, 8, 2, 'Pull with your arms.'),
       ('Deadlifts',     '../assets/images/Deadlift.png', 2, 1, 3, 3, 8, 2, 'Pull with your arms.'),
       ('Dumbbell Rows', '../assets/images/Kneeling\ Dumbbell\ Row.png', 2, 1, 3, 3, 8, 2, 'Pull with your arms.');


# Advanced Bodybuilding 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Incline Bench', '../assets/images/INCLINEBarbell-Bench-Press.png', 3, 1, 1, 3, 4, 1, 'Push with your arms.'),
       ('Decline Bench', '../assets/images/DECLINEBarbell-Bench-Press.png', 3, 1, 1, 3, 4, 1, 'Push with your arms.'),
       ('Flat Bench',    '../assets/images/FLATBarbell-Bench-Press.png', 3, 1, 1, 3, 4, 1, 'Push with your arms.'),
       ('Cable Flyes',   '../assets/images/Cable\ Flies.png', 3, 1, 1, 3, 4, 1, 'Push with your arms.');

# Advanced Bodybuilding 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',      '../assets/images/Barbell\ Squat.png', 3, 1, 2, 3, 4, 1, 'Push with your legs.'),
       ('Leg Press',  '../assets/images/Leg\ Press.png', 3, 1, 2, 3, 4, 1, 'Push with your legs.'),
       ('Calf Raise', '../assets/images/Calf\ Raises.png', 3, 1, 2, 3, 4, 1, 'Push with your legs.'),
       ('Leg Curls',  '../assets/images/Leg\ Curls.png', 3, 1, 2, 3, 4, 1, 'Push with your legs.');

# Advanced Bodybuilding 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Seated Rows',   '../assets/images/Seated\ Rows.png', 3, 1, 3, 3, 4, 1, 'Pull with your arms.'),
       ('Pullups',       '../assets/images/underhand\ pull-up.jpg', 3, 1, 3, 3, 4, 1, 'Pull with your arms.'),
       ('Deadlifts',     '../assets/images/Deadlift.png', 3, 1, 3, 3, 4, 1, 'Pull with your arms.'),
       ('Dumbbell Rows', '../assets/images/Kneeling\ Dumbbell\ Row.png', 3, 1, 3, 3, 4, 1, 'Pull with your arms.');

###### Powerlifting Workouts #####

# Beginner Powerlifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bench Press', '../assets/images/FLATBarbell-Bench-Press.png', 1, 2, 1, 3, 12, -2, 'Push with your arms.'),
       ('Calf Raise',  '../assets/images/Calf\ Raises.png', 1, 2, 1, 3, 12, -2, 'Push with your legs.');

# Beginner Powerlifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Deadlift',        '../assets/images/Deadlift.png', 1, 2, 2, 3, 12, -2, 'Push with your legs.'),
       ('Dumbbell Squat',  '../assets/images/dumbbell-squat.png', 1, 2, 2, 3, 12, -2, 'Push with your legs.');

# Beginner Powerlifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',         '../assets/images/Barbell\ Squat.png', 1, 2, 3, 3, 12, -2, 'Push with your legs.'),
       ('Farmers Walks', '../assets/images/farmers-walk.png', 1, 2, 3, 3, 12, -2, 'Push with your legs.');


# Intermediate Powerlifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bench Press', '../assets/images/FLATBarbell-Bench-Press.png', 2, 2, 1, 3, 11, -2, 'Push with your arms.'),
       ('Calf Raise',  '../assets/images/Calf\ Raises.png', 2, 2, 1, 3, 11, -2, 'Push with your legs.');

# Intermediate Powerlifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Deadlift',        '../assets/images/Deadlift.png', 2, 2, 2, 3, 11, -2, 'Push with your legs.'),
       ('Dumbbell Squat',  '../assets/images/dumbbell-squat.png', 2, 2, 2, 3, 11, -2, 'Push with your legs.');

# Intermediate Powerlifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',         '../assets/images/Barbell\ Squat.png', 2, 2, 3, 3, 11, -2, 'Push with your legs.'),
       ('Farmers Walks', '../assets/images/farmers-walk.png', 2, 2, 3, 3, 11, -2, 'Push with your legs.');


# Advanced Powerlifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bench Press', '../assets/images/FLATBarbell-Bench-Press.png', 3, 2, 1, 3, 10, -2, 'Push with your arms.'),
       ('Calf Raise',  '../assets/images/Calf\ Raises.png', 3, 2, 1, 3, 10, -2, 'Push with your legs.');

# Advanced Powerlifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Deadlift',        '../assets/images/Deadlift.png', 3, 2, 2, 3, 10, -2, 'Push with your legs.'),
       ('Dumbbell Squat',  '../assets/images/dumbbell-squat.png', 3, 2, 2, 3, 10, -2, 'Push with your legs.');

# Advanced Powerlifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat',         '../assets/images/Barbell\ Squat.png', 3, 2, 3, 3, 10, -2, 'Push with your legs.'),
       ('Farmers Walks', '../assets/images/farmers-walk.png', 3, 2, 3, 3, 10, -2, 'Push with your legs.');



# ##### Olympic Lifting #####

# Beginner Olympic Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Snatch',    '../assets/images/snatch.jpeg', 1, 3, 1, 3, 9, -2, 'Pull it high'),
       ('Push Ups',  '../assets/images/Push-Ups.png', 1, 3, 1, 2, 20, 0, 'Pull it high');

# Beginner Olympic Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Press', '../assets/images/push\ press.png', 1, 3, 2, 3, 9, -2, 'Pull it high'),
       ('Squat Jump', '../assets/images/Squat\ Jump.png', 1, 3, 2, 2, 20, 0, 'Pull it high');

# Beginner Olympic Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Hang Clean', '../assets/images/hang\ clean.jpeg', 1, 3, 3, 3, 9, -2, 'Pull it high'),
       ('Plank',      '../assets/images/Plank.png', 1, 3, 3, 2, 20, 0, 'Pull it high');


# Intermediate Olympic Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Snatch',    '../assets/images/snatch.jpeg', 2, 3, 1, 3, 7, -2, 'Pull it high'),
       ('Push Ups',  '../assets/images/Push-Ups.png', 2, 3, 1, 2, 15, 0, 'Pull it high');

# Intermediate Olympic Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Press', '../assets/images/push\ press.png', 2, 3, 2, 3, 7, -2, 'Pull it high'),
       ('Squat Jump', '../assets/images/Squat\ Jump.png', 2, 3, 2, 2, 15, 0, 'Pull it high');

# Intermediate Olympic Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Hang Clean', '../assets/images/hang\ clean.jpeg', 2, 3, 3, 3, 7, -2, 'Pull it high'),
       ('Plank',      '../assets/images/Plank.png', 2, 3, 3, 2, 15, 0, 'Pull it high');


# Advanced Olympic Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Snatch',    '../assets/images/snatch.jpeg', 3, 3, 1, 3, 5, -2, 'Pull it high'),
       ('Push Ups',  '../assets/images/Push-Ups.png', 3, 3, 1, 2, 10, 0, 'Pull it high');

# Advanced Olympic Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Press', '../assets/images/push\ press.png', 3, 3, 2, 3, 5, -2, 'Pull it high'),
       ('Squat Jump', '../assets/images/Squat\ Jump.png', 3, 3, 2, 2, 10, 0, 'Pull it high');

# Advanced Olympic Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Hang Clean', '../assets/images/hang\ clean.jpeg', 3, 3, 3, 3, 5, -2, 'Pull it high'),
       ('Plank',      '../assets/images/Plank.png', 3, 3, 3, 2, 10, 0, 'Pull it high');



# ##### Circuit #####

# Beginner Circuit 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Ups',      '../assets/images/Push-Ups.png', 1, 4, 1, 3, 10, 5, 'Hold tight'),
       ('Plank',         '../assets/images/Plank.png', 1, 4, 1, 3, 30, 15, 'Hold tight'),
       ('Walking Lunge', '../assets/images/Walking\ lunges.png', 1, 4, 1, 3, 16, 4, 'Hold tight'),
       ('Burpees',       '../assets/images/Burpees.jpg', 1, 4, 1, 3, 10, 2, 'Hold tight');

# Beginner Circuit 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bodyweight Squat', '../assets/images/Bodyweight\ Squats.png', 1, 4, 2, 3, 10, 5, 'Hold tight'),
       ('Crunches',         '../assets/images/Crunch.png', 1, 4, 2, 3, 30, 15, 'Hold tight'),
       ('Pull Ups',         '../assets/images/underhand\ pull-up.jpg', 1, 4, 2, 3, 16, 4, 'Hold tight'),
       ('Mtn Climber',      '../assets/images/Mountain-Climbers.jpg', 1, 4, 2, 3, 10, 2, 'Hold tight');

# Beginner Circuit 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat Jump', '../assets/images/Squat\ Jump.png', 1, 4, 3, 3, 10, 5, 'Hold tight'),
       ('Step Ups',   '../assets/images/dumbbell-step-up.png', 1, 4, 3, 3, 30, 15, 'Hold tight'),
       ('Ball Slams', '../assets/images/ball-slams.png', 1, 4, 3, 3, 16, 4, 'Hold tight'),
       ('Leg Lifts',  '../assets/images/laying\ leg\ raise.png', 1, 4, 3, 3, 10, 2, 'Hold tight');


# Intermediate Circuit 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Ups',      '../assets/images/Push-Ups.png', 2, 4, 1, 3, 20, 5, 'Hold tight'),
       ('Plank',         '../assets/images/Plank.png', 2, 4, 1, 3, 30, 15, 'Hold tight'),
       ('Walking Lunge', '../assets/images/Walking\ lunges.png', 2, 4, 1, 3, 20, 4, 'Hold tight'),
       ('Burpees',       '../assets/images/Burpees.jpg', 2, 4, 1, 3, 10, 2, 'Hold tight');

# Intermediate Circuit 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bodyweight Squat', '../assets/images/Bodyweight\ Squats.png', 2, 4, 2, 3, 20, 5, 'Hold tight'),
       ('Crunches',         '../assets/images/Crunch.png', 2, 4, 2, 3, 20, 15, 'Hold tight'),
       ('Pull Ups',         '../assets/images/underhand\ pull-up.jpg', 2, 4, 2, 3, 20, 4, 'Hold tight'),
       ('Mtn Climber',      '../assets/images/Mountain-Climbers.jpg', 2, 4, 2, 3, 10, 2, 'Hold tight');

# Intermediate Circuit 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat Jump', '../assets/images/Squat\ Jump.png', 2, 4, 3, 3, 20, 5, 'Hold tight'),
       ('Step Ups',   '../assets/images/dumbbell-step-up.png', 2, 4, 3, 3, 30, 15, 'Hold tight'),
       ('Ball Slams', '../assets/images/ball-slams.png', 2, 4, 3, 3, 20, 4, 'Hold tight'),
       ('Leg Lifts',  '../assets/images/laying\ leg\ raise.png', 2, 4, 3, 3, 10, 2, 'Hold tight');


# Advanced Circuit 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Push Ups',      '../assets/images/Push-Ups.png', 3, 4, 1, 3, 30, 5, 'Hold tight'),
       ('Plank',         '../assets/images/Plank.png', 3, 4, 1, 3, 40, 15, 'Hold tight'),
       ('Walking Lunge', '../assets/images/Walking\ lunges.png', 3, 4, 1, 3, 30, 4, 'Hold tight'),
       ('Burpees',       '../assets/images/Burpees.jpg', 3, 4, 1, 3, 20, 2, 'Hold tight');

# Advanced Circuit 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Bodyweight Squat', '../assets/images/Bodyweight\ Squats.png', 3, 4, 2, 3, 30, 5, 'Hold tight'),
       ('Crunches',         '../assets/images/Crunch.png', 3, 4, 2, 3, 40, 15, 'Hold tight'),
       ('Pull Ups',         '../assets/images/underhand\ pull-up.jpg', 3, 4, 2, 3, 30, 4, 'Hold tight'),
       ('Mtn Climber',      '../assets/images/Mountain-Climbers.jpg', 3, 4, 2, 3, 20, 2, 'Hold tight');

# Advanced Circuit 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squat Jump', '../assets/images/Squat\ Jump.png', 3, 4, 3, 3, 30, 5, 'Hold tight'),
       ('Step Ups',   '../assets/images/dumbbell-step-up.png', 3, 4, 3, 3, 40, 15, 'Hold tight'),
       ('Ball Slams', '../assets/images/ball-slams.png', 3, 4, 3, 3, 30, 4, 'Hold tight'),
       ('Leg Lifts',  '../assets/images/laying\ leg\ raise.png', 3, 4, 3, 3, 20, 2, 'Hold tight');



# ##### Yoga #####

# Beginner Yoga Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Downward-Facing Dog', '../assets/images/downwardfacingdogs.jpeg', 1, 5, 1, 10, 10, 0, 'Yoga is fun. Hold it tight.'),
       ('Mountain Pose',       '../assets/images/mountain\ pose.png', 1, 5, 1, 10, 10, 0, 'Yoga is fun. Hold it tight.');

# Beginner Yoga Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Crescent Lunge', '../assets/images/crescent\ lunge.jpeg', 1, 5, 2, 10, 10, 0, 'Yoga is fun. Hold it tight.'),
       ('Warrior II',     '../assets/images/warriorII\ pose.jpeg', 1, 5, 2, 10, 10, 0, 'Yoga is fun. Hold it tight.');

# Beginner Yoga Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Tree',         '../assets/images/tree\ pose.jpeg', 1, 5, 3, 10, 10, 0, 'Yoga is fun. Hold it tight.'),
       ('Dancers Pose', '../assets/images/dancers\ pose.jpg', 1, 5, 3, 10, 10, 0, 'Yoga is fun. Hold it tight.');


# Intermediate Yoga Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Downward-Facing Dog', '../assets/images/downwardfacingdogs.jpeg', 2, 5, 1, 10, 20, 0, 'Yoga is fun. Hold it tight.'),
       ('Mountain Pose',       '../assets/images/mountain\ pose.png', 2, 5, 1, 10, 20, 0, 'Yoga is fun. Hold it tight.');

# Intermediate Yoga Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Crescent Lunge', '../assets/images/crescent\ lunge.jpeg', 2, 5, 2, 10, 20, 0, 'Yoga is fun. Hold it tight.'),
       ('Warrior II',     '../assets/images/warriorII\ pose.jpeg', 2, 5, 2, 10, 20, 0, 'Yoga is fun. Hold it tight.');

# Intermediate Yoga Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Tree',         '../assets/images/tree\ pose.jpeg', 2, 5, 3, 10, 20, 0, 'Yoga is fun. Hold it tight.'),
       ('Dancers Pose', '../assets/images/dancers\ pose.jpg', 2, 5, 3, 10, 20, 0, 'Yoga is fun. Hold it tight.');


# Advanced Yoga Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Downward-Facing Dog', '../assets/images/downwardfacingdogs.jpeg', 3, 5, 1, 10, 30, 0, 'Yoga is fun. Hold it tight.'),
       ('Mountain Pose',       '../assets/images/mountain\ pose.png', 3, 5, 1, 10, 30, 0, 'Yoga is fun. Hold it tight.');

# Advanced Yoga Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Crescent Lunge', '../assets/images/crescent\ lunge.jpeg', 3, 5, 2, 10, 30, 0, 'Yoga is fun. Hold it tight.'),
       ('Warrior II',     '../assets/images/warriorII\ pose.jpeg', 3, 5, 2, 10, 30, 0, 'Yoga is fun. Hold it tight.');

# Advanced Yoga Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Tree',         '../assets/images/tree\ pose.jpeg', 3, 5, 3, 10, 30, 0, 'Yoga is fun. Hold it tight.'),
       ('Dancers Pose', '../assets/images/dancers\ pose.jpg', 3, 5, 3, 10, 30, 0, 'Yoga is fun. Hold it tight.');



# ##### P90X #####

# Beginner P90X Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Shoulder Press',    '../assets/images/shoulder\ press.png', 1, 6, 1, 1, 50, 0, 'Does anyone do this still?');

# Beginner P90X Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Romanian Deadlift', '../assets/images/romaniandeadlift.png', 1, 6, 2, 1, 50, 0, 'Does anyone do this still?');

# Beginner P90X Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squats',            '../assets/images/Bodyweight\ Squats.png', 1, 6, 3, 1, 50, 0, 'Does anyone do this still?');


# Intermediate P90X Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Shoulder Press',    '../assets/images/shoulder\ press.png', 2, 6, 1, 1, 100, 0, 'Does anyone do this still?');

# Intermediate P90X Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Romanian Deadlift', '../assets/images/romaniandeadlift.png', 2, 6, 2, 1, 100, 0, 'Does anyone do this still?');

# Intermediate P90X Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squats',            '../assets/images/Bodyweight\ Squats.png', 2, 6, 3, 1, 100, 0, 'Does anyone do this still?');


# Advanced P90X Lifting 'A'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Shoulder Press',    '../assets/images/shoulder\ press.png', 3, 6, 1, 1, 150, 0, 'Does anyone do this still?');

# Advanced P90X Lifting 'B'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Romanian Deadlift', '../assets/images/romaniandeadlift.png', 3, 6, 2, 1, 150, 0, 'Does anyone do this still?');

# Advanced P90X Lifting 'C'
INSERT INTO exercise (name, image, intensity_id, type_id, workout_id, sets, reps, delta, description)
VALUES ('Squats',            '../assets/images/Bodyweight\ Squats.png', 3, 6, 3, 1, 150, 0, 'Does anyone do this still?');



# ##### Forum #####

INSERT INTO forum (post)
VALUES ('This is a sample first post.'),
       ('This is a sample second post. :)');
