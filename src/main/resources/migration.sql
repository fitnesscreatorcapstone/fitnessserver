USE fitness_db;

CREATE TABLE intensity
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  level VARCHAR(16),
  CONSTRAINT pk_intensity PRIMARY KEY (id)
);

CREATE TABLE type
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(32),
  CONSTRAINT pk_type PRIMARY KEY (id)
);

CREATE TABLE workout_option
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  workout VARCHAR(8),
  CONSTRAINT pk_type PRIMARY KEY (id)
);

CREATE TABLE exercise
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(64),
  image VARCHAR(64),
  intensity_id INT UNSIGNED,
  type_id INT UNSIGNED,
  workout_id INT UNSIGNED,
  sets INT,
  reps INT,
  delta INT,
  description VARCHAR(256),
  PRIMARY KEY (id),
  CONSTRAINT FK7b8d5ispodnssa4yq4mo35q5m
  FOREIGN KEY (intensity_id) REFERENCES intensity(id),
  CONSTRAINT FK64w6v4r6qmcfahxttg2wi5q0c
  FOREIGN KEY (type_id) REFERENCES type(id),
  CONSTRAINT FK5rfen3x5ut1nkvudq3dedls46
  FOREIGN KEY (workout_id) REFERENCES workout_option(id)
);

CREATE TABLE news
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  event VARCHAR(256),
  description VARCHAR(256),
  CONSTRAINT pk_news PRIMARY KEY (id)
);

CREATE TABLE forum
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  post VARCHAR(256),
  CONSTRAINT pk_forum PRIMARY KEY (id)
);

CREATE TABLE user_workouts
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  date TIMESTAMP,
  user_id bigint NOT NULL,
  intensity_id INT UNSIGNED NOT NULL,
  type_id INT UNSIGNED NOT NULL,
  workout_id INT UNSIGNED NOT NULL,
  CONSTRAINT pk_user_workouts PRIMARY KEY (id),
  CONSTRAINT FK3kvixeh1lmycrxyhu3jx9169h FOREIGN KEY (user_id) REFERENCES users(id),
  CONSTRAINT FKmbrlqn7hservja8lg1wt6f3b8 FOREIGN KEY (intensity_id) REFERENCES intensity(id),
  CONSTRAINT FKaojtw9dp408ee5388bvnnrp7l FOREIGN KEY (type_id) REFERENCES type(id),
  CONSTRAINT FKrfd0ue036ojxqe2jgcav49rx1 FOREIGN KEY (workout_id) REFERENCES workout_option(id)
);
