package com.fitness.fitnessserver.Model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Getter
@Setter
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String image;

    @JoinColumn(name = "intensityId", referencedColumnName = "id")
    @ManyToOne
    private Intensity intensityId;

    @JoinColumn(name = "typeId", referencedColumnName = "id")
    @ManyToOne
    private Type typeId;

    @JoinColumn(name = "workoutId", referencedColumnName = "id")
    @ManyToOne
    private WorkoutOption workoutId;

    @Column
    private int sets;   // Good

    @Column
    private int reps;

    @Column
    private int delta;

    @Column
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Intensity getIntensityId() {
        return intensityId;
    }

    public void setIntensityId(Intensity intensityId) {
        this.intensityId = intensityId;
    }

    public Type getTypeId() {
        return typeId;
    }

    public void setTypeId(Type typeId) {
        this.typeId = typeId;
    }

    public WorkoutOption getWorkoutId() {
        return workoutId;
    }

    public void setWorkoutId(WorkoutOption workoutId) {
        this.workoutId = workoutId;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
