package com.fitness.fitnessserver.Model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
public class UserWorkouts
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Timestamp date;

    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne
    private User userId;

    @JoinColumn(name = "intensityId", referencedColumnName = "id")
    @ManyToOne
    private Intensity intensityId;

    @JoinColumn(name = "typeId", referencedColumnName = "id")
    @ManyToOne
    private Type typeId;

    @JoinColumn(name = "workoutId", referencedColumnName = "id")
    @ManyToOne
    private WorkoutOption workoutId;

}
