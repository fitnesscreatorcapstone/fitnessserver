package com.fitness.fitnessserver.Model;

import lombok.*;
import javax.persistence.*;


@Entity
@Getter
@Setter
public class WorkoutOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column
    public String workout;
}
