package com.fitness.fitnessserver.Model;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
public class RoleNames {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

}
