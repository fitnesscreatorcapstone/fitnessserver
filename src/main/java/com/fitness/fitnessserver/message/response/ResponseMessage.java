package com.fitness.fitnessserver.message.response;

import lombok.*;

@Getter
@Setter
public class ResponseMessage {
    private String message;

    public ResponseMessage(String message) {
        this.message = message;
    }
}
