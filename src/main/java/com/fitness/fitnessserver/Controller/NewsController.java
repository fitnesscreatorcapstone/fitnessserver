package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.Exercise;
import com.fitness.fitnessserver.Model.News;
import com.fitness.fitnessserver.Repository.ExerciseRepository;
import com.fitness.fitnessserver.Repository.NewsRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class NewsController {
    private final NewsRepository newsRepo;

    private NewsController(NewsRepository newsRepo) { this.newsRepo = newsRepo; }

    @GetMapping("/news")
    public List<News> getAllNews() {
        return (List<News>) this.newsRepo.findAll();
    }
}
