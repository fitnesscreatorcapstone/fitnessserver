package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.User;
import com.fitness.fitnessserver.Model.UserWorkouts;
import com.fitness.fitnessserver.Repository.UserRepository;
import com.fitness.fitnessserver.Repository.UserWorkoutsRepository;

import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@RestController
@CrossOrigin
public class UserWorkoutsController
{
    private final UserWorkoutsRepository userWorkoutsRepo;
    private final UserRepository userRepo;

    private UserWorkoutsController(UserWorkoutsRepository userWorkoutsRepo, UserRepository userRepo) {
        this.userWorkoutsRepo = userWorkoutsRepo;
        this.userRepo = userRepo;
    }

    @GetMapping("/userWorkouts")
    public List<UserWorkouts> getAllExercises() {
        return (List<UserWorkouts>) this.userWorkoutsRepo.findAll();
    }

    @GetMapping("/user/{username}")
    public Long getUserId(@PathVariable String username) {
        User newUser = userRepo.getByUsername(username);
        return newUser.getId();
    }

    @PostMapping("/selectedUserWorkouts")
    public UserWorkouts addSelectedWorkout(@RequestBody UserWorkouts userWorkouts) {
       Calendar now = GregorianCalendar.getInstance();
       Timestamp createdDate = new Timestamp(now.getTimeInMillis());
       userWorkouts.setDate(createdDate);
       userWorkoutsRepo.save(userWorkouts);
       return  userWorkouts;

    }

    @GetMapping("/userSelectedWorkouts/{userId}")
    public List<UserWorkouts> getAllUserWorkouts(@PathVariable Long userId) {
        return this.userWorkoutsRepo.getExerciseByIntensityTypeOption(userId);
    }
}
