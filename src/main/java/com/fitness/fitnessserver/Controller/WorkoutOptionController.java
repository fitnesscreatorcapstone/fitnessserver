package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.WorkoutOption;
import com.fitness.fitnessserver.Repository.WorkoutOptionRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class WorkoutOptionController {
    private final WorkoutOptionRepository workoutOptionRepo;

    private WorkoutOptionController(WorkoutOptionRepository workoutOptionRepo) { this.workoutOptionRepo = workoutOptionRepo; }

    @RequestMapping("/workoutoptions")
    public List<WorkoutOption> getAllWorkouts() {
        return (List<WorkoutOption>) this.workoutOptionRepo.findAll();
    }
}
