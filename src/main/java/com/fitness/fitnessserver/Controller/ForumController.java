package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.Forum;
import com.fitness.fitnessserver.Repository.ForumRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ForumController
{
    private final ForumRepository forumRepo;

    private ForumController(ForumRepository forumRepo) { this.forumRepo = forumRepo; }

    @GetMapping("/forums")
    public List<Forum> getAllPosts() { return (List<Forum>) this.forumRepo.findAll(); }

    @PostMapping("/forums")
    public Iterable<Forum> createItem(@RequestBody Forum item){
        this.forumRepo.save(item);
        return this.forumRepo.findAll();
    }
}
