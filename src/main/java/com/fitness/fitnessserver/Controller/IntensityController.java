package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.Intensity;
import com.fitness.fitnessserver.Repository.IntensityRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class IntensityController {
    private final IntensityRepository intensityRepo;

    private IntensityController(IntensityRepository intensityRepo) { this.intensityRepo = intensityRepo; }

    @RequestMapping("/intensities")
    public List<Intensity> getAllIntensities() { return (List<Intensity>) this.intensityRepo.findAll(); }
}
