package com.fitness.fitnessserver.Controller;

import com.fitness.fitnessserver.Model.Exercise;
import com.fitness.fitnessserver.Model.Intensity;
import com.fitness.fitnessserver.Model.Type;
import com.fitness.fitnessserver.Model.WorkoutOption;
import com.fitness.fitnessserver.Repository.ExerciseRepository;
import org.springframework.web.bind.annotation.*;

import java.io.Console;
import java.util.List;

@RestController
@CrossOrigin
public class ExerciseController
{
    private final ExerciseRepository exerciseRepo;

    private ExerciseController(ExerciseRepository exerciseRepo) { this.exerciseRepo = exerciseRepo; }

    @GetMapping("/exercises")
    public List<Exercise> getAllExercises() {
        return (List<Exercise>) this.exerciseRepo.findAll();
    }

//    @GetMapping("/workout/{id1}/{id2}/{id3}")
//    public List<Exercise> getWorkout(@PathVariable long id1, @PathVariable long id2, @PathVariable long id3) {
////        System.out.println(this.exerciseRepo.getExerciseByIntensityTypeOption(id1, id2, id3));
//        return this.exerciseRepo.getExerciseByIntensityIdEqualsid1AndTypeIdEqualsid2AndWorkoutIdEqualsid3(id1, id2, id3);
//    }

    @GetMapping("/workout/{id1}/{id2}/{id3}")
    public List<Exercise> getSingleWorkout(@PathVariable Intensity id1, @PathVariable Type id2, @PathVariable WorkoutOption id3) {
        return this.exerciseRepo.getAllByIntensityIdEqualsAndTypeIdEqualsAndWorkoutIdEquals(id1, id2, id3);
    }
}
