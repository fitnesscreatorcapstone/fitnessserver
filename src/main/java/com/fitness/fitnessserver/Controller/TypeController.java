package com.fitness.fitnessserver.Controller;


import com.fitness.fitnessserver.Model.Type;

import com.fitness.fitnessserver.Repository.TypeRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class TypeController {
    private final TypeRepository typeRepo;

    private TypeController(TypeRepository typeRepo) {this.typeRepo = typeRepo;}

    @RequestMapping("/types")
    public List<Type> getAllTypes() {
        return (List<Type>) typeRepo.findAll();
    }
}
