package com.fitness.fitnessserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessserverApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(FitnessserverApplication.class, args);
    }

}
