package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.UserWorkouts;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserWorkoutsRepository extends CrudRepository<UserWorkouts, Long> {
    @Query(value = "Select * From user_workouts u Where u.user_id = ?1 ORDER BY u.date" , nativeQuery = true)
    List<UserWorkouts> getExerciseByIntensityTypeOption(Long id1);
}
