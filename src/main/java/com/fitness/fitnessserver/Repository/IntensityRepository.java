package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.Intensity;
import org.springframework.data.repository.CrudRepository;

public interface IntensityRepository extends CrudRepository<Intensity, Long> {
}
