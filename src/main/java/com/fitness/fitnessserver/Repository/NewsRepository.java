package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.News;
import org.springframework.data.repository.CrudRepository;

public interface NewsRepository extends CrudRepository<News, Long> {
}
