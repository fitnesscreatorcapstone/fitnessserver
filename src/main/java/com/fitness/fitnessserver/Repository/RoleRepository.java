package com.fitness.fitnessserver.Repository;

import java.util.Optional;

import com.fitness.fitnessserver.Model.Role;
import com.fitness.fitnessserver.Model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
