package com.fitness.fitnessserver.Repository;

import org.springframework.data.repository.CrudRepository;
import com.fitness.fitnessserver.Model.Type;

public interface TypeRepository extends CrudRepository<Type, Long> {
}
