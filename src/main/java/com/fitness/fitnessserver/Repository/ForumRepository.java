package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.Forum;
import org.springframework.data.repository.CrudRepository;

public interface ForumRepository extends CrudRepository<Forum, Long>
{

}
