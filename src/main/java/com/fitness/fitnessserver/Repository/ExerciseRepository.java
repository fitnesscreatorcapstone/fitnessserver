package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.Exercise;
import com.fitness.fitnessserver.Model.Intensity;
import com.fitness.fitnessserver.Model.Type;
import com.fitness.fitnessserver.Model.WorkoutOption;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ExerciseRepository extends CrudRepository<Exercise, Long> {

//    @Query(value = "Select * From Exercise e Where e.intensity_id = ?1 and e.type_id = ?2 and e.workout_id = ?3", nativeQuery = true)
//    List<Exercise> getExerciseByIntensityTypeOption(Long id1, Long id2,Long id3);

//    List<Exercise> getExerciseByIntensityIdEqualsid1AndTypeIdEqualsid2AndWorkoutIdEqualsid3(Long id1, Long id2, Long id3);
    List<Exercise> getAllByIntensityIdEqualsAndTypeIdEqualsAndWorkoutIdEquals(Intensity id1, Type id2, WorkoutOption id3);

//    @Query(value = "select * from Exercise where Exercise.intensityId = ?1 and Exercise.typeId = ?2 and Exercise.workoutId = ?3", nativeQuery = true)
//    List<Exercise> getExerciseByIntensityId(Long id1, Long id2, Long id3);
}
