package com.fitness.fitnessserver.Repository;

import com.fitness.fitnessserver.Model.WorkoutOption;
import org.springframework.data.repository.CrudRepository;

public interface WorkoutOptionRepository extends CrudRepository<WorkoutOption, Long> {
}
